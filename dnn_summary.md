# **What Is Neural Network :**  
* Neural network is a circuit of artificial neurons or it can be said as a network of artificial nuerons, also reffered to as _Nodes_ . 
* This type of network can be used at places where they can be trained by means of a dataset or a predefined collection of information.
* Can be used to store and process information, for predictive modelling, classification and for other things such as image recognition, speech recognition, e.t.c.

![image](https://gitlab.com/Vishwas2304/s-block/-/raw/master/images/Screenshot__65_.png)

## Parts of the Network :  

**_A basic neural network consists of an input layer, hidden layer 1, hidden layer 2, and the output or prediction layer._** 

![](https://gitlab.com/Vishwas2304/s-block/-/raw/master/images/Screenshot__68_.png)

## Types of neural network :  
* Feed forward network : One of the simplest network, where the data or the input travels in one direction.
* Radial basis network
* Self organising neural network.
* Recurrent Networks
* Convolution Neural networks : similar to Feed forward, also here nuerons learnable weights.

# Gradient Descent :  
 * This is basically a optimisation algorithm which can be used to find the minimum of a differentiable fuction. 
 * Gradient descent is also used to   update the parameters in a model.  
 ![](https://gitlab.com/Vishwas2304/s-block/-/raw/master/images/Screenshot__70_.png)

## Basic formula of Gradient descent :
 ![](https://gitlab.com/Vishwas2304/s-block/-/raw/master/images/Screenshot__72_.png)

## Steps to calculate the minimium value of a Function using gradient : 

So, Gradient descent is basically an optimisation algorithm of fuctions , this can be done in the following steps : -  
* step 1 : Start with random value of parameters and calculate the prediction errors.
* step 2 : Calculate the change in parameters and the errors
* step 3 : Adjust the parameters using the gradient to minimise the error value
* step 4 : Take new values of parameters and repeat the above process untill the error there is no significant change in the error value.  

## Learning Rate :  
The size of these steps while calculating the minimum value of a function is called the learning rate. With a high learning rate we can cover more ground each step, but there is a risk of missing the minimum point. While,  With a very low learning rate, we can confidently move in the direction of the negative gradient, but this is more time consuming. Overall,  a low learning rate is more precise.

# How do neural networks learn :  
![](https://gitlab.com/Vishwas2304/s-block/-/raw/master/images/Screenshot__74_.png)

### The learning algorithm is as follows:  
* Start with random values for the network parameters.
* Pass the parameters through the network to obtain their prediction.
* Compare these predictions obtained with the expected values and calculate the error.
* Perform the backpropagation in order to propagate this loss to each of the parameters that make up the model.
* Use this information to update the parameters of the neural network with the gradient descent in a way that the total loss is reduced.
* Repeat the previous steps until we get a good model.

# What is Backpropogation :
* Back propogation is the reverse process of forward propogation.
* It is the process by which the error calculated in each step are sent back through the entire network.
* Backpropagation takes the error associated with a less accurate prediction by a network, and uses that error to adjust the neural network’s    parameters in the direction of less error.

### This happens step by step:

* The network makes a prediction about data, using the input values.
* The error of the prediction is calculated
* The error is backpropagated to adjust the parameters to a better value.

![](https://gitlab.com/Vishwas2304/s-block/-/raw/master/images/Screenshot__76_.png)

## Advantage and disadvantage of Backpropogation :

![](https://gitlab.com/Vishwas2304/s-block/-/raw/master/images/Screenshot__80_.png)




